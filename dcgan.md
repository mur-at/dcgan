
### porting of https://github.com/pytorch/examples/blob/master/dcgan/main.py to jupyter notebook


```python
from __future__ import print_function
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import matplotlib.pyplot as plt
import numpy as np
```


```python
# we fake a class options here to replace what would be read from parser.parse_args

class Options(object):
    def __init__(self): 
        self.dataset = "folder"
        self.dataroot = "inputdata"
        self.workers = 4
        self.batchSize = 32
        self.imageSize = 64
        self.nz = 64                   # size of the latent z vector to create noise
        self.ngf = 64
        self.ndf = 64
        self.niter = 200               # number of epochs to train for
        self.lr = 0.0002               # learning rate
        self.beta1 = 0.5               # beta1 for adam.
        self.cuda = False              # enables cuda
        self.ngpu = 0                  # number of gpus to use
        self.netG = ""                 # path to netG (to continue training)
        self.netD = ""                 # path to netD (to continue training)
        self.outf = "outputdata"       # folder to output images and model checkpoints
        self.manualSeed = None         # manual seed
```


```python
def show(img, fs=(6,6)):
    plt.figure(figsize=fs)
    plt.imshow(np.transpose((img / 2 + 0.5).clamp(0, 1).numpy(), (1, 2, 0)), interpolation='nearest')

```


```python
opt = Options()

try:
    os.makedirs(opt.outf)
except OSError:
    pass
```


```python

if opt.manualSeed is None:
    opt.manualSeed = random.randint(1, 10000)
print("Random Seed: ", opt.manualSeed)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)

cudnn.benchmark = True
```

    Random Seed:  2965



```python
if torch.cuda.is_available() and not opt.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")
```


```python
if opt.dataset in ['imagenet', 'folder', 'lfw']:
    # folder dataset
    dataset = dset.ImageFolder(root=opt.dataroot,
                               transform=transforms.Compose([
                                   transforms.Resize(opt.imageSize),
                                   transforms.CenterCrop(opt.imageSize),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                               ]))

elif opt.dataset == 'lsun':
    dataset = dset.LSUN(root=opt.dataroot, classes=['bedroom_train'],
                        transform=transforms.Compose([
                            transforms.Resize(opt.imageSize),
                            transforms.CenterCrop(opt.imageSize),
                            transforms.ToTensor(),
                            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                        ]))
    
elif opt.dataset == 'cifar10':
    dataset = dset.CIFAR10(root=opt.dataroot, download=True,
                           transform=transforms.Compose([
                               transforms.Resize(opt.imageSize),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                           ]))
    
elif opt.dataset == 'fake':
    dataset = dset.FakeData(image_size=(3, opt.imageSize, opt.imageSize),
                            transform=transforms.ToTensor())

assert dataset
dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize,
                                         shuffle=True, num_workers=int(opt.workers))

print('Training on %d from %d available images.' % (len(dataloader), len(dataset)))
```

    Training on 507 from 16200 available images.



```python
device = torch.device("cuda:0" if opt.cuda else "cpu")
ngpu = int(opt.ngpu)
nz = int(opt.nz)
ngf = int(opt.ngf)
ndf = int(opt.ndf)
nc = 3
```


```python

# custom weights initialization called on netG and netD
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)
```


```python

class Generator(nn.Module):
    def __init__(self, ngpu):
        super(Generator, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is Z, going into a convolution
            nn.ConvTranspose2d(     nz, ngf * 8, 4, 1, 0, bias=False),
            nn.BatchNorm2d(ngf * 8),
            nn.ReLU(True),
            # state size. (ngf*8) x 4 x 4
            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),
            # state size. (ngf*4) x 8 x 8
            nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),
            # state size. (ngf*2) x 16 x 16
            nn.ConvTranspose2d(ngf * 2,     ngf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),
            # state size. (ngf) x 32 x 32
            nn.ConvTranspose2d(    ngf,      nc, 4, 2, 1, bias=False),
            nn.Tanh()
            # state size. (nc) x 64 x 64
        )

    def forward(self, input):
        if input.is_cuda and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)
        return output
```


```python
netG = Generator(ngpu).to(device)
netG.apply(weights_init)
if opt.netG != '':
    netG.load_state_dict(torch.load(opt.netG))
print(netG)
```

    Generator(
      (main): Sequential(
        (0): ConvTranspose2d(64, 512, kernel_size=(4, 4), stride=(1, 1), bias=False)
        (1): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (2): ReLU(inplace)
        (3): ConvTranspose2d(512, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (4): BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (5): ReLU(inplace)
        (6): ConvTranspose2d(256, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (7): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (8): ReLU(inplace)
        (9): ConvTranspose2d(128, 64, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (10): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (11): ReLU(inplace)
        (12): ConvTranspose2d(64, 3, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (13): Tanh()
      )
    )



```python
class Discriminator(nn.Module):
    def __init__(self, ngpu):
        super(Discriminator, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(nc, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )

    def forward(self, input):
        if input.is_cuda and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)

        return output.view(-1, 1).squeeze(1)
```


```python

netD = Discriminator(ngpu).to(device)
netD.apply(weights_init)
if opt.netD != '':
    netD.load_state_dict(torch.load(opt.netD))
print(netD)
```

    Discriminator(
      (main): Sequential(
        (0): Conv2d(3, 64, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (1): LeakyReLU(negative_slope=0.2, inplace)
        (2): Conv2d(64, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (3): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (4): LeakyReLU(negative_slope=0.2, inplace)
        (5): Conv2d(128, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (6): BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (7): LeakyReLU(negative_slope=0.2, inplace)
        (8): Conv2d(256, 512, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
        (9): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        (10): LeakyReLU(negative_slope=0.2, inplace)
        (11): Conv2d(512, 1, kernel_size=(4, 4), stride=(1, 1), bias=False)
        (12): Sigmoid()
      )
    )



```python

criterion = nn.BCELoss()

fixed_noise = torch.randn(opt.batchSize, nz, 1, 1, device=device)
real_label = 1
fake_label = 0

# setup optimizer
optimizerD = optim.Adam(netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
optimizerG = optim.Adam(netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
```


```python
for epoch in range(opt.niter):
    for i, data in enumerate(dataloader, 0):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        # train with real
        netD.zero_grad()
        real_cpu = data[0].to(device)
        batch_size = real_cpu.size(0)
        label = torch.full((batch_size,), real_label, device=device)

        output = netD(real_cpu)
        errD_real = criterion(output, label)
        errD_real.backward()
        D_x = output.mean().item()

        # train with fake
        noise = torch.randn(batch_size, nz, 1, 1, device=device)
        fake = netG(noise)
        label.fill_(fake_label)
        output = netD(fake.detach())
        errD_fake = criterion(output, label)
        errD_fake.backward()
        D_G_z1 = output.mean().item()
        errD = errD_real + errD_fake
        optimizerD.step()

        ############################
        # (2) Update G network: maximize log(D(G(z)))
        ###########################
        netG.zero_grad()
        label.fill_(real_label)  # fake labels are real for generator cost
        output = netD(fake)
        errG = criterion(output, label)
        errG.backward()
        D_G_z2 = output.mean().item()
        optimizerG.step()

        if (i + 1) % 50 == 0:
            print('[%2d/%2d][%4d/%4d] Loss_D: %.4f Loss_G: %.4f D(x): %.4f D(G(z)): %.4f / %.4f'
                  % (epoch + 1, opt.niter, i + 1, len(dataloader),
                     errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))
            #fake = netG(fixed_noise)
            #vutils.save_image(fake.detach(),
            #                  '%s/%03d_%03d_generated.png' % (opt.outf, epoch + 1, i +  1),
            #                  normalize=True)
            #          
            #vutils.save_image(real_cpu,
            #                  '%s/%03d_%03d_real.png' % (opt.outf, epoch + 1, i + 1),
            #                  normalize=True)
        

    fake = netG(fixed_noise)
    vutils.save_image(fake.detach(),
                      '%s/epoch_%03d_generated.png' % (opt.outf, epoch + 1),
                      normalize=True)

    # do checkpointing
    torch.save(netG.state_dict(), '%s/netG_epoch_%03d.pth' % (opt.outf, epoch))
    torch.save(netD.state_dict(), '%s/netD_epoch_%03d.pth' % (opt.outf, epoch))
    
print('Done!')
```

    [ 1/200][  50/ 507] Loss_D: 0.4372 Loss_G: 9.5544 D(x): 0.8744 D(G(z)): 0.1685 / 0.0002
    [ 1/200][ 100/ 507] Loss_D: 1.0891 Loss_G: 6.5300 D(x): 0.5504 D(G(z)): 0.0189 / 0.0045
    [ 1/200][ 150/ 507] Loss_D: 0.6410 Loss_G: 9.2575 D(x): 0.6307 D(G(z)): 0.0037 / 0.0004
    [ 1/200][ 200/ 507] Loss_D: 0.5734 Loss_G: 2.5803 D(x): 0.7289 D(G(z)): 0.0305 / 0.1160
    [ 1/200][ 250/ 507] Loss_D: 0.1884 Loss_G: 4.7366 D(x): 0.9096 D(G(z)): 0.0535 / 0.0415
    [ 1/200][ 300/ 507] Loss_D: 0.6825 Loss_G: 7.9779 D(x): 0.8849 D(G(z)): 0.3650 / 0.0012
    [ 1/200][ 350/ 507] Loss_D: 0.3742 Loss_G: 6.1386 D(x): 0.7830 D(G(z)): 0.0325 / 0.0092
    [ 1/200][ 400/ 507] Loss_D: 0.2152 Loss_G: 6.7286 D(x): 0.8777 D(G(z)): 0.0632 / 0.0033
    [ 1/200][ 450/ 507] Loss_D: 0.7199 Loss_G: 5.6803 D(x): 0.8573 D(G(z)): 0.3489 / 0.0048
    [ 1/200][ 500/ 507] Loss_D: 0.5959 Loss_G: 5.7739 D(x): 0.9559 D(G(z)): 0.3778 / 0.0058
    [ 2/200][  50/ 507] Loss_D: 1.0038 Loss_G: 4.2744 D(x): 0.5288 D(G(z)): 0.0353 / 0.0205
    [ 2/200][ 100/ 507] Loss_D: 0.7614 Loss_G: 8.8599 D(x): 0.9753 D(G(z)): 0.4314 / 0.0003
    [ 2/200][ 150/ 507] Loss_D: 0.3442 Loss_G: 4.3368 D(x): 0.8923 D(G(z)): 0.1717 / 0.0163
    [ 2/200][ 200/ 507] Loss_D: 0.4079 Loss_G: 5.0891 D(x): 0.7552 D(G(z)): 0.0674 / 0.0096
    [ 2/200][ 250/ 507] Loss_D: 0.3021 Loss_G: 5.2038 D(x): 0.8540 D(G(z)): 0.1014 / 0.0077
    [ 2/200][ 300/ 507] Loss_D: 0.7557 Loss_G: 5.5752 D(x): 0.8209 D(G(z)): 0.3684 / 0.0062
    [ 2/200][ 350/ 507] Loss_D: 0.1839 Loss_G: 6.8355 D(x): 0.8641 D(G(z)): 0.0193 / 0.0021
    [ 2/200][ 400/ 507] Loss_D: 0.5077 Loss_G: 4.9294 D(x): 0.8171 D(G(z)): 0.2092 / 0.0097
    [ 2/200][ 450/ 507] Loss_D: 0.5910 Loss_G: 4.3389 D(x): 0.7234 D(G(z)): 0.1712 / 0.0203
    [ 2/200][ 500/ 507] Loss_D: 0.4746 Loss_G: 5.0931 D(x): 0.7903 D(G(z)): 0.1565 / 0.0091
    [ 3/200][  50/ 507] Loss_D: 0.4998 Loss_G: 3.8363 D(x): 0.8013 D(G(z)): 0.1938 / 0.0322
    [ 3/200][ 100/ 507] Loss_D: 0.9454 Loss_G: 4.8629 D(x): 0.6837 D(G(z)): 0.2865 / 0.0125
    [ 3/200][ 150/ 507] Loss_D: 0.5291 Loss_G: 5.1573 D(x): 0.8092 D(G(z)): 0.2225 / 0.0076
    [ 3/200][ 200/ 507] Loss_D: 0.4369 Loss_G: 3.9108 D(x): 0.8024 D(G(z)): 0.1444 / 0.0289
    [ 3/200][ 250/ 507] Loss_D: 0.4376 Loss_G: 5.0994 D(x): 0.8914 D(G(z)): 0.2311 / 0.0091
    [ 3/200][ 300/ 507] Loss_D: 0.6410 Loss_G: 4.1699 D(x): 0.7857 D(G(z)): 0.2683 / 0.0212
    [ 3/200][ 350/ 507] Loss_D: 0.2140 Loss_G: 4.9235 D(x): 0.8803 D(G(z)): 0.0666 / 0.0125
    [ 3/200][ 400/ 507] Loss_D: 0.4750 Loss_G: 4.2121 D(x): 0.7189 D(G(z)): 0.0704 / 0.0260
    [ 3/200][ 450/ 507] Loss_D: 0.8486 Loss_G: 7.1214 D(x): 0.8681 D(G(z)): 0.4240 / 0.0013
    [ 3/200][ 500/ 507] Loss_D: 0.8365 Loss_G: 6.8669 D(x): 0.9134 D(G(z)): 0.4573 / 0.0015
    [ 4/200][  50/ 507] Loss_D: 0.4213 Loss_G: 4.7389 D(x): 0.8757 D(G(z)): 0.2071 / 0.0131
    [ 4/200][ 100/ 507] Loss_D: 0.4430 Loss_G: 4.9925 D(x): 0.8762 D(G(z)): 0.2261 / 0.0131
    [ 4/200][ 150/ 507] Loss_D: 0.3878 Loss_G: 5.1621 D(x): 0.8889 D(G(z)): 0.2017 / 0.0083
    [ 4/200][ 200/ 507] Loss_D: 0.3880 Loss_G: 3.5337 D(x): 0.7400 D(G(z)): 0.0442 / 0.0392
    [ 4/200][ 250/ 507] Loss_D: 0.7466 Loss_G: 6.9880 D(x): 0.9312 D(G(z)): 0.3986 / 0.0015
    [ 4/200][ 300/ 507] Loss_D: 0.7022 Loss_G: 3.3414 D(x): 0.6464 D(G(z)): 0.0187 / 0.0524
    [ 4/200][ 350/ 507] Loss_D: 0.3996 Loss_G: 5.0200 D(x): 0.8272 D(G(z)): 0.1384 / 0.0123
    [ 4/200][ 400/ 507] Loss_D: 0.5866 Loss_G: 6.9635 D(x): 0.8400 D(G(z)): 0.2684 / 0.0014
    [ 4/200][ 450/ 507] Loss_D: 0.3768 Loss_G: 5.5621 D(x): 0.8863 D(G(z)): 0.1902 / 0.0058
    [ 4/200][ 500/ 507] Loss_D: 0.4843 Loss_G: 5.7542 D(x): 0.9295 D(G(z)): 0.2887 / 0.0052
    [ 5/200][  50/ 507] Loss_D: 0.3366 Loss_G: 4.4442 D(x): 0.8741 D(G(z)): 0.1654 / 0.0150
    [ 5/200][ 100/ 507] Loss_D: 0.4440 Loss_G: 3.8456 D(x): 0.7816 D(G(z)): 0.1258 / 0.0384
    [ 5/200][ 150/ 507] Loss_D: 0.4859 Loss_G: 5.9332 D(x): 0.9099 D(G(z)): 0.2868 / 0.0038
    [ 5/200][ 200/ 507] Loss_D: 0.3555 Loss_G: 6.9229 D(x): 0.9474 D(G(z)): 0.2265 / 0.0013
    [ 5/200][ 250/ 507] Loss_D: 0.3436 Loss_G: 4.1965 D(x): 0.7469 D(G(z)): 0.0128 / 0.0350
    [ 5/200][ 300/ 507] Loss_D: 0.3529 Loss_G: 4.9600 D(x): 0.8465 D(G(z)): 0.1283 / 0.0120
    [ 5/200][ 350/ 507] Loss_D: 0.3642 Loss_G: 5.0821 D(x): 0.7666 D(G(z)): 0.0195 / 0.0135
    [ 5/200][ 400/ 507] Loss_D: 0.4684 Loss_G: 4.6975 D(x): 0.7621 D(G(z)): 0.1153 / 0.0191
    [ 5/200][ 450/ 507] Loss_D: 0.4657 Loss_G: 6.2460 D(x): 0.9324 D(G(z)): 0.2709 / 0.0034
    [ 5/200][ 500/ 507] Loss_D: 0.2206 Loss_G: 5.5840 D(x): 0.9055 D(G(z)): 0.0982 / 0.0065
    [ 6/200][  50/ 507] Loss_D: 0.3478 Loss_G: 4.8313 D(x): 0.8707 D(G(z)): 0.1506 / 0.0130
    [ 6/200][ 100/ 507] Loss_D: 0.3667 Loss_G: 4.6848 D(x): 0.8446 D(G(z)): 0.1334 / 0.0158
    [ 6/200][ 150/ 507] Loss_D: 0.2009 Loss_G: 5.8657 D(x): 0.8996 D(G(z)): 0.0614 / 0.0068
    [ 6/200][ 200/ 507] Loss_D: 0.1589 Loss_G: 3.9888 D(x): 0.9067 D(G(z)): 0.0492 / 0.0278
    [ 6/200][ 250/ 507] Loss_D: 0.2057 Loss_G: 4.7892 D(x): 0.8396 D(G(z)): 0.0114 / 0.0167
    [ 6/200][ 300/ 507] Loss_D: 0.3359 Loss_G: 5.0721 D(x): 0.7739 D(G(z)): 0.0338 / 0.0161
    [ 6/200][ 350/ 507] Loss_D: 0.2743 Loss_G: 4.8559 D(x): 0.8893 D(G(z)): 0.1259 / 0.0112
    [ 6/200][ 400/ 507] Loss_D: 0.1425 Loss_G: 5.2165 D(x): 0.9666 D(G(z)): 0.0937 / 0.0091
    [ 6/200][ 450/ 507] Loss_D: 0.1733 Loss_G: 4.3337 D(x): 0.9106 D(G(z)): 0.0701 / 0.0183
    [ 6/200][ 500/ 507] Loss_D: 0.0696 Loss_G: 6.7237 D(x): 0.9513 D(G(z)): 0.0166 / 0.0025
    [ 7/200][  50/ 507] Loss_D: 0.1668 Loss_G: 5.1921 D(x): 0.9179 D(G(z)): 0.0703 / 0.0090
    [ 7/200][ 100/ 507] Loss_D: 0.4204 Loss_G: 8.6342 D(x): 0.9819 D(G(z)): 0.2901 / 0.0003
    [ 7/200][ 150/ 507] Loss_D: 0.5192 Loss_G: 4.2170 D(x): 0.7753 D(G(z)): 0.1085 / 0.0281
    [ 7/200][ 200/ 507] Loss_D: 0.1405 Loss_G: 4.7455 D(x): 0.9622 D(G(z)): 0.0872 / 0.0134
    [ 7/200][ 250/ 507] Loss_D: 0.1497 Loss_G: 4.3036 D(x): 0.9307 D(G(z)): 0.0689 / 0.0171
    [ 7/200][ 300/ 507] Loss_D: 0.1405 Loss_G: 5.1253 D(x): 0.9348 D(G(z)): 0.0619 / 0.0107
    [ 7/200][ 350/ 507] Loss_D: 0.1678 Loss_G: 4.6931 D(x): 0.9055 D(G(z)): 0.0533 / 0.0145
    [ 7/200][ 400/ 507] Loss_D: 0.3190 Loss_G: 6.9113 D(x): 0.9337 D(G(z)): 0.1962 / 0.0015
    [ 7/200][ 450/ 507] Loss_D: 0.2059 Loss_G: 5.1153 D(x): 0.9623 D(G(z)): 0.1340 / 0.0093
    [ 7/200][ 500/ 507] Loss_D: 0.3349 Loss_G: 2.7602 D(x): 0.8231 D(G(z)): 0.0785 / 0.0927
    [ 8/200][  50/ 507] Loss_D: 0.1655 Loss_G: 3.6753 D(x): 0.8863 D(G(z)): 0.0247 / 0.0674
    [ 8/200][ 100/ 507] Loss_D: 0.1456 Loss_G: 3.6702 D(x): 0.9038 D(G(z)): 0.0351 / 0.0454
    [ 8/200][ 150/ 507] Loss_D: 0.5005 Loss_G: 3.4750 D(x): 0.6662 D(G(z)): 0.0110 / 0.0547
    [ 8/200][ 200/ 507] Loss_D: 0.2739 Loss_G: 7.2661 D(x): 0.9672 D(G(z)): 0.1863 / 0.0012
    [ 8/200][ 250/ 507] Loss_D: 0.2484 Loss_G: 7.5320 D(x): 0.9776 D(G(z)): 0.1815 / 0.0008
    [ 8/200][ 300/ 507] Loss_D: 0.3119 Loss_G: 6.5143 D(x): 0.9245 D(G(z)): 0.1641 / 0.0024
    [ 8/200][ 350/ 507] Loss_D: 0.3358 Loss_G: 7.1595 D(x): 0.9259 D(G(z)): 0.1828 / 0.0015
    [ 8/200][ 400/ 507] Loss_D: 0.2235 Loss_G: 5.6083 D(x): 0.9417 D(G(z)): 0.1291 / 0.0053
    [ 8/200][ 450/ 507] Loss_D: 0.3465 Loss_G: 7.8904 D(x): 0.9685 D(G(z)): 0.2388 / 0.0007
    [ 8/200][ 500/ 507] Loss_D: 0.1489 Loss_G: 4.5610 D(x): 0.8953 D(G(z)): 0.0212 / 0.0203
    [ 9/200][  50/ 507] Loss_D: 0.2362 Loss_G: 7.0990 D(x): 0.9725 D(G(z)): 0.1692 / 0.0013
    [ 9/200][ 100/ 507] Loss_D: 0.1382 Loss_G: 4.2884 D(x): 0.8994 D(G(z)): 0.0238 / 0.0275
    [ 9/200][ 150/ 507] Loss_D: 0.1598 Loss_G: 4.9595 D(x): 0.9138 D(G(z)): 0.0579 / 0.0113
    [ 9/200][ 200/ 507] Loss_D: 0.1468 Loss_G: 5.3893 D(x): 0.9353 D(G(z)): 0.0658 / 0.0069
    [ 9/200][ 250/ 507] Loss_D: 0.2637 Loss_G: 3.3120 D(x): 0.8199 D(G(z)): 0.0269 / 0.0669
    [ 9/200][ 300/ 507] Loss_D: 0.1756 Loss_G: 5.4471 D(x): 0.9495 D(G(z)): 0.0976 / 0.0071
    [ 9/200][ 350/ 507] Loss_D: 1.8281 Loss_G: 3.2383 D(x): 0.4022 D(G(z)): 0.0960 / 0.0747
    [ 9/200][ 400/ 507] Loss_D: 0.2515 Loss_G: 4.8235 D(x): 0.8956 D(G(z)): 0.1037 / 0.0124
    [ 9/200][ 450/ 507] Loss_D: 0.1868 Loss_G: 4.8153 D(x): 0.9387 D(G(z)): 0.0976 / 0.0117
    [ 9/200][ 500/ 507] Loss_D: 0.2074 Loss_G: 4.4913 D(x): 0.9197 D(G(z)): 0.0937 / 0.0165
    [10/200][  50/ 507] Loss_D: 0.1711 Loss_G: 4.7142 D(x): 0.9096 D(G(z)): 0.0610 / 0.0135
    [10/200][ 100/ 507] Loss_D: 0.1334 Loss_G: 4.4662 D(x): 0.9302 D(G(z)): 0.0442 / 0.0191
    [10/200][ 150/ 507] Loss_D: 0.1612 Loss_G: 4.4766 D(x): 0.8921 D(G(z)): 0.0134 / 0.0263
    [10/200][ 200/ 507] Loss_D: 0.1054 Loss_G: 5.0413 D(x): 0.9419 D(G(z)): 0.0381 / 0.0116
    [10/200][ 250/ 507] Loss_D: 0.1803 Loss_G: 4.5521 D(x): 0.9245 D(G(z)): 0.0894 / 0.0153
    [10/200][ 300/ 507] Loss_D: 0.1516 Loss_G: 4.4939 D(x): 0.9100 D(G(z)): 0.0495 / 0.0173
    [10/200][ 350/ 507] Loss_D: 0.1665 Loss_G: 5.9527 D(x): 0.9776 D(G(z)): 0.1144 / 0.0039
    [10/200][ 400/ 507] Loss_D: 0.2173 Loss_G: 6.3024 D(x): 0.9544 D(G(z)): 0.1425 / 0.0025
    [10/200][ 450/ 507] Loss_D: 0.2861 Loss_G: 4.4319 D(x): 0.8216 D(G(z)): 0.0071 / 0.0324
    [10/200][ 500/ 507] Loss_D: 0.6157 Loss_G: 3.9662 D(x): 0.6918 D(G(z)): 0.1102 / 0.0336
    [11/200][  50/ 507] Loss_D: 0.1415 Loss_G: 4.6408 D(x): 0.9476 D(G(z)): 0.0753 / 0.0146
    [11/200][ 100/ 507] Loss_D: 0.1068 Loss_G: 5.1998 D(x): 0.9452 D(G(z)): 0.0429 / 0.0109
    [11/200][ 150/ 507] Loss_D: 0.1141 Loss_G: 5.0902 D(x): 0.9474 D(G(z)): 0.0517 / 0.0101
    [11/200][ 200/ 507] Loss_D: 0.0791 Loss_G: 4.4583 D(x): 0.9634 D(G(z)): 0.0388 / 0.0180
    [11/200][ 250/ 507] Loss_D: 0.0953 Loss_G: 4.6417 D(x): 0.9456 D(G(z)): 0.0353 / 0.0172
    [11/200][ 300/ 507] Loss_D: 0.0948 Loss_G: 5.8136 D(x): 0.9801 D(G(z)): 0.0669 / 0.0050
    [11/200][ 350/ 507] Loss_D: 0.0814 Loss_G: 4.9507 D(x): 0.9509 D(G(z)): 0.0281 / 0.0136
    [11/200][ 400/ 507] Loss_D: 0.0841 Loss_G: 4.6425 D(x): 0.9619 D(G(z)): 0.0418 / 0.0142
    [11/200][ 450/ 507] Loss_D: 0.1046 Loss_G: 4.3898 D(x): 0.9386 D(G(z)): 0.0361 / 0.0202
    [11/200][ 500/ 507] Loss_D: 0.1678 Loss_G: 6.4050 D(x): 0.9600 D(G(z)): 0.1092 / 0.0024
    [12/200][  50/ 507] Loss_D: 0.0726 Loss_G: 5.3186 D(x): 0.9737 D(G(z)): 0.0428 / 0.0062
    [12/200][ 100/ 507] Loss_D: 0.0860 Loss_G: 5.2542 D(x): 0.9621 D(G(z)): 0.0407 / 0.0086
    [12/200][ 150/ 507] Loss_D: 0.1011 Loss_G: 5.7911 D(x): 0.9801 D(G(z)): 0.0718 / 0.0043
    [12/200][ 200/ 507] Loss_D: 0.0698 Loss_G: 5.0050 D(x): 0.9541 D(G(z)): 0.0178 / 0.0114
    [12/200][ 250/ 507] Loss_D: 0.2102 Loss_G: 5.7000 D(x): 0.8970 D(G(z)): 0.0772 / 0.0060
    [12/200][ 300/ 507] Loss_D: 0.1912 Loss_G: 5.0426 D(x): 0.9220 D(G(z)): 0.0865 / 0.0104
    [12/200][ 350/ 507] Loss_D: 0.1704 Loss_G: 3.7995 D(x): 0.8916 D(G(z)): 0.0425 / 0.0329
    [12/200][ 400/ 507] Loss_D: 0.0930 Loss_G: 4.6042 D(x): 0.9640 D(G(z)): 0.0503 / 0.0158
    [12/200][ 450/ 507] Loss_D: 0.1103 Loss_G: 4.6929 D(x): 0.9424 D(G(z)): 0.0383 / 0.0128
    [12/200][ 500/ 507] Loss_D: 0.1194 Loss_G: 4.8487 D(x): 0.9559 D(G(z)): 0.0596 / 0.0113
    [13/200][  50/ 507] Loss_D: 0.0792 Loss_G: 4.7767 D(x): 0.9512 D(G(z)): 0.0268 / 0.0122
    [13/200][ 100/ 507] Loss_D: 0.0420 Loss_G: 4.8830 D(x): 0.9721 D(G(z)): 0.0126 / 0.0117
    [13/200][ 150/ 507] Loss_D: 0.0701 Loss_G: 4.5082 D(x): 0.9559 D(G(z)): 0.0230 / 0.0162
    [13/200][ 200/ 507] Loss_D: 0.0896 Loss_G: 3.8220 D(x): 0.9376 D(G(z)): 0.0204 / 0.0343
    [13/200][ 250/ 507] Loss_D: 0.0683 Loss_G: 5.1837 D(x): 0.9787 D(G(z)): 0.0436 / 0.0090
    [13/200][ 300/ 507] Loss_D: 0.0748 Loss_G: 4.3809 D(x): 0.9507 D(G(z)): 0.0217 / 0.0174
    [13/200][ 350/ 507] Loss_D: 0.0640 Loss_G: 4.9248 D(x): 0.9665 D(G(z)): 0.0286 / 0.0111
    [13/200][ 400/ 507] Loss_D: 0.1010 Loss_G: 6.4425 D(x): 0.9932 D(G(z)): 0.0800 / 0.0025
    [13/200][ 450/ 507] Loss_D: 0.0541 Loss_G: 5.1957 D(x): 0.9741 D(G(z)): 0.0253 / 0.0073
    [13/200][ 500/ 507] Loss_D: 0.0716 Loss_G: 5.7028 D(x): 0.9802 D(G(z)): 0.0474 / 0.0053
    [14/200][  50/ 507] Loss_D: 0.0490 Loss_G: 5.0238 D(x): 0.9856 D(G(z)): 0.0334 / 0.0077
    [14/200][ 100/ 507] Loss_D: 0.0355 Loss_G: 4.9477 D(x): 0.9830 D(G(z)): 0.0178 / 0.0095
    [14/200][ 150/ 507] Loss_D: 0.0458 Loss_G: 5.3513 D(x): 0.9784 D(G(z)): 0.0227 / 0.0082
    [14/200][ 200/ 507] Loss_D: 0.0563 Loss_G: 5.1537 D(x): 0.9864 D(G(z)): 0.0401 / 0.0068
    [14/200][ 250/ 507] Loss_D: 0.0601 Loss_G: 6.0150 D(x): 0.9635 D(G(z)): 0.0205 / 0.0049
    [14/200][ 300/ 507] Loss_D: 0.0249 Loss_G: 4.7924 D(x): 0.9895 D(G(z)): 0.0140 / 0.0110
    [14/200][ 350/ 507] Loss_D: 0.0601 Loss_G: 4.6886 D(x): 0.9696 D(G(z)): 0.0259 / 0.0134
    [14/200][ 400/ 507] Loss_D: 0.0482 Loss_G: 5.1098 D(x): 0.9632 D(G(z)): 0.0094 / 0.0094
    [14/200][ 450/ 507] Loss_D: 0.0469 Loss_G: 4.8674 D(x): 0.9716 D(G(z)): 0.0166 / 0.0131
    [14/200][ 500/ 507] Loss_D: 0.0239 Loss_G: 6.1181 D(x): 0.9845 D(G(z)): 0.0080 / 0.0039
    [15/200][  50/ 507] Loss_D: 1.6805 Loss_G: 2.6495 D(x): 0.5719 D(G(z)): 0.3528 / 0.1020
    [15/200][ 100/ 507] Loss_D: 1.5649 Loss_G: 12.6493 D(x): 0.8872 D(G(z)): 0.5409 / 0.0000
    [15/200][ 150/ 507] Loss_D: 0.2347 Loss_G: 6.1641 D(x): 0.9149 D(G(z)): 0.0941 / 0.0049
    [15/200][ 200/ 507] Loss_D: 0.2868 Loss_G: 4.2043 D(x): 0.8751 D(G(z)): 0.0906 / 0.0236
    [15/200][ 250/ 507] Loss_D: 0.0905 Loss_G: 5.0195 D(x): 0.9448 D(G(z)): 0.0278 / 0.0116
    [15/200][ 300/ 507] Loss_D: 0.1322 Loss_G: 4.2202 D(x): 0.9166 D(G(z)): 0.0366 / 0.0246
    [15/200][ 350/ 507] Loss_D: 0.1145 Loss_G: 4.8007 D(x): 0.9326 D(G(z)): 0.0368 / 0.0122
    [15/200][ 400/ 507] Loss_D: 0.1717 Loss_G: 6.3622 D(x): 0.9933 D(G(z)): 0.1287 / 0.0026
    [15/200][ 450/ 507] Loss_D: 0.0562 Loss_G: 4.6744 D(x): 0.9643 D(G(z)): 0.0186 / 0.0145
    [15/200][ 500/ 507] Loss_D: 0.0630 Loss_G: 4.9173 D(x): 0.9769 D(G(z)): 0.0376 / 0.0093
    [16/200][  50/ 507] Loss_D: 0.0698 Loss_G: 4.6590 D(x): 0.9731 D(G(z)): 0.0406 / 0.0130
    [16/200][ 100/ 507] Loss_D: 0.0369 Loss_G: 4.9076 D(x): 0.9888 D(G(z)): 0.0248 / 0.0109
    [16/200][ 150/ 507] Loss_D: 0.0410 Loss_G: 5.4161 D(x): 0.9890 D(G(z)): 0.0285 / 0.0071
    [16/200][ 200/ 507] Loss_D: 0.0336 Loss_G: 5.6258 D(x): 0.9853 D(G(z)): 0.0183 / 0.0052
    [16/200][ 250/ 507] Loss_D: 0.0465 Loss_G: 5.1254 D(x): 0.9844 D(G(z)): 0.0287 / 0.0080
    [16/200][ 300/ 507] Loss_D: 0.0233 Loss_G: 5.3551 D(x): 0.9906 D(G(z)): 0.0135 / 0.0071
    [16/200][ 350/ 507] Loss_D: 0.0403 Loss_G: 4.8780 D(x): 0.9782 D(G(z)): 0.0173 / 0.0124
    [16/200][ 400/ 507] Loss_D: 0.0433 Loss_G: 4.8401 D(x): 0.9721 D(G(z)): 0.0142 / 0.0138
    [16/200][ 450/ 507] Loss_D: 0.0215 Loss_G: 5.2214 D(x): 0.9875 D(G(z)): 0.0088 / 0.0076
    [16/200][ 500/ 507] Loss_D: 0.0246 Loss_G: 5.4850 D(x): 0.9882 D(G(z)): 0.0125 / 0.0077
    [17/200][  50/ 507] Loss_D: 0.0261 Loss_G: 4.9802 D(x): 0.9882 D(G(z)): 0.0141 / 0.0092
    [17/200][ 100/ 507] Loss_D: 0.0231 Loss_G: 5.7983 D(x): 0.9922 D(G(z)): 0.0150 / 0.0045
    [17/200][ 150/ 507] Loss_D: 0.0270 Loss_G: 5.5586 D(x): 0.9784 D(G(z)): 0.0050 / 0.0065
    [17/200][ 200/ 507] Loss_D: 0.0341 Loss_G: 5.7562 D(x): 0.9866 D(G(z)): 0.0198 / 0.0042
    [17/200][ 250/ 507] Loss_D: 0.0256 Loss_G: 5.5579 D(x): 0.9971 D(G(z)): 0.0221 / 0.0053
    [17/200][ 300/ 507] Loss_D: 0.0556 Loss_G: 6.4042 D(x): 0.9954 D(G(z)): 0.0477 / 0.0022
    [17/200][ 350/ 507] Loss_D: 0.0269 Loss_G: 5.5057 D(x): 0.9857 D(G(z)): 0.0121 / 0.0071
    [17/200][ 400/ 507] Loss_D: 0.0236 Loss_G: 5.7158 D(x): 0.9866 D(G(z)): 0.0099 / 0.0051
    [17/200][ 450/ 507] Loss_D: 0.0272 Loss_G: 5.5121 D(x): 0.9861 D(G(z)): 0.0128 / 0.0055
    [17/200][ 500/ 507] Loss_D: 0.0201 Loss_G: 5.5181 D(x): 0.9886 D(G(z)): 0.0084 / 0.0064
    [18/200][  50/ 507] Loss_D: 1.3630 Loss_G: 4.1703 D(x): 0.8215 D(G(z)): 0.5106 / 0.0578
    [18/200][ 100/ 507] Loss_D: 1.3059 Loss_G: 1.4361 D(x): 0.5543 D(G(z)): 0.3089 / 0.3081
    [18/200][ 150/ 507] Loss_D: 1.2827 Loss_G: 2.5927 D(x): 0.7927 D(G(z)): 0.4678 / 0.1197
    [18/200][ 200/ 507] Loss_D: 1.4823 Loss_G: 1.0500 D(x): 0.4396 D(G(z)): 0.2295 / 0.4260
    [18/200][ 250/ 507] Loss_D: 1.2508 Loss_G: 2.5130 D(x): 0.6331 D(G(z)): 0.2843 / 0.1390
    [18/200][ 300/ 507] Loss_D: 1.4716 Loss_G: 2.8743 D(x): 0.6179 D(G(z)): 0.4175 / 0.0894
    [18/200][ 350/ 507] Loss_D: 0.8969 Loss_G: 5.0916 D(x): 0.7819 D(G(z)): 0.2773 / 0.0100
    [18/200][ 400/ 507] Loss_D: 0.6077 Loss_G: 4.1785 D(x): 0.7290 D(G(z)): 0.1399 / 0.0236
    [18/200][ 450/ 507] Loss_D: 0.8321 Loss_G: 5.4650 D(x): 0.8214 D(G(z)): 0.3019 / 0.0066
    [18/200][ 500/ 507] Loss_D: 0.5112 Loss_G: 3.1795 D(x): 0.7096 D(G(z)): 0.0270 / 0.1001
    [19/200][  50/ 507] Loss_D: 0.2235 Loss_G: 5.2320 D(x): 0.9227 D(G(z)): 0.0840 / 0.0112
    [19/200][ 100/ 507] Loss_D: 0.2170 Loss_G: 4.4476 D(x): 0.8532 D(G(z)): 0.0368 / 0.0176
    [19/200][ 150/ 507] Loss_D: 0.0979 Loss_G: 4.8227 D(x): 0.9527 D(G(z)): 0.0451 / 0.0106
    [19/200][ 200/ 507] Loss_D: 0.0941 Loss_G: 4.4171 D(x): 0.9494 D(G(z)): 0.0381 / 0.0166
    [19/200][ 250/ 507] Loss_D: 0.0891 Loss_G: 4.1651 D(x): 0.9530 D(G(z)): 0.0361 / 0.0239
    [19/200][ 300/ 507] Loss_D: 0.1019 Loss_G: 5.0004 D(x): 0.9618 D(G(z)): 0.0552 / 0.0108
    [19/200][ 350/ 507] Loss_D: 0.0710 Loss_G: 5.4266 D(x): 0.9675 D(G(z)): 0.0341 / 0.0062
    [19/200][ 400/ 507] Loss_D: 0.0607 Loss_G: 5.1839 D(x): 0.9709 D(G(z)): 0.0291 / 0.0079
    [19/200][ 450/ 507] Loss_D: 0.0552 Loss_G: 4.3033 D(x): 0.9673 D(G(z)): 0.0209 / 0.0192
    [19/200][ 500/ 507] Loss_D: 0.0622 Loss_G: 5.1131 D(x): 0.9649 D(G(z)): 0.0251 / 0.0104
    [20/200][  50/ 507] Loss_D: 0.0536 Loss_G: 4.7752 D(x): 0.9656 D(G(z)): 0.0174 / 0.0139
    [20/200][ 100/ 507] Loss_D: 0.0332 Loss_G: 5.0308 D(x): 0.9750 D(G(z)): 0.0077 / 0.0085
    [20/200][ 150/ 507] Loss_D: 0.0460 Loss_G: 5.1333 D(x): 0.9712 D(G(z)): 0.0156 / 0.0089
    [20/200][ 200/ 507] Loss_D: 0.0474 Loss_G: 4.8072 D(x): 0.9830 D(G(z)): 0.0292 / 0.0119
    [20/200][ 250/ 507] Loss_D: 0.0434 Loss_G: 4.9406 D(x): 0.9791 D(G(z)): 0.0214 / 0.0098
    [20/200][ 300/ 507] Loss_D: 0.0431 Loss_G: 5.3505 D(x): 0.9877 D(G(z)): 0.0286 / 0.0071
    [20/200][ 350/ 507] Loss_D: 0.0315 Loss_G: 4.9768 D(x): 0.9869 D(G(z)): 0.0179 / 0.0092
    [20/200][ 400/ 507] Loss_D: 0.0362 Loss_G: 5.0651 D(x): 0.9853 D(G(z)): 0.0204 / 0.0095
    [20/200][ 450/ 507] Loss_D: 0.0329 Loss_G: 5.5617 D(x): 0.9829 D(G(z)): 0.0153 / 0.0061
    [20/200][ 500/ 507] Loss_D: 0.0243 Loss_G: 5.2982 D(x): 0.9925 D(G(z)): 0.0164 / 0.0075
    [21/200][  50/ 507] Loss_D: 0.5793 Loss_G: 6.1354 D(x): 0.7144 D(G(z)): 0.0144 / 0.0124
    [21/200][ 100/ 507] Loss_D: 0.0936 Loss_G: 4.6987 D(x): 0.9444 D(G(z)): 0.0315 / 0.0138
    [21/200][ 150/ 507] Loss_D: 0.0609 Loss_G: 5.6581 D(x): 0.9560 D(G(z)): 0.0139 / 0.0076
    [21/200][ 200/ 507] Loss_D: 0.0490 Loss_G: 5.0837 D(x): 0.9760 D(G(z)): 0.0235 / 0.0095
    [21/200][ 250/ 507] Loss_D: 0.0369 Loss_G: 5.1859 D(x): 0.9842 D(G(z)): 0.0196 / 0.0081
    [21/200][ 300/ 507] Loss_D: 0.0522 Loss_G: 4.5709 D(x): 0.9687 D(G(z)): 0.0193 / 0.0153
    [21/200][ 350/ 507] Loss_D: 0.0356 Loss_G: 5.3800 D(x): 0.9826 D(G(z)): 0.0173 / 0.0063
    [21/200][ 400/ 507] Loss_D: 0.0338 Loss_G: 5.1530 D(x): 0.9821 D(G(z)): 0.0152 / 0.0093
    [21/200][ 450/ 507] Loss_D: 0.0347 Loss_G: 5.2196 D(x): 0.9770 D(G(z)): 0.0108 / 0.0077
    [21/200][ 500/ 507] Loss_D: 0.0205 Loss_G: 5.7902 D(x): 0.9909 D(G(z)): 0.0112 / 0.0052
    [22/200][  50/ 507] Loss_D: 0.0219 Loss_G: 5.5471 D(x): 0.9900 D(G(z)): 0.0116 / 0.0068
    [22/200][ 100/ 507] Loss_D: 0.0264 Loss_G: 5.5955 D(x): 0.9851 D(G(z)): 0.0111 / 0.0065
    [22/200][ 150/ 507] Loss_D: 0.0307 Loss_G: 5.7442 D(x): 0.9942 D(G(z)): 0.0237 / 0.0049
    [22/200][ 200/ 507] Loss_D: 0.0252 Loss_G: 5.3019 D(x): 0.9858 D(G(z)): 0.0105 / 0.0073
    [22/200][ 250/ 507] Loss_D: 0.0152 Loss_G: 5.7299 D(x): 0.9912 D(G(z)): 0.0063 / 0.0055
    [22/200][ 300/ 507] Loss_D: 0.0167 Loss_G: 6.0808 D(x): 0.9886 D(G(z)): 0.0052 / 0.0035
    [22/200][ 350/ 507] Loss_D: 0.0160 Loss_G: 5.7201 D(x): 0.9928 D(G(z)): 0.0087 / 0.0046
    [22/200][ 400/ 507] Loss_D: 0.0232 Loss_G: 5.6914 D(x): 0.9912 D(G(z)): 0.0141 / 0.0049
    [22/200][ 450/ 507] Loss_D: 0.0242 Loss_G: 5.7489 D(x): 0.9948 D(G(z)): 0.0185 / 0.0046
    [22/200][ 500/ 507] Loss_D: 0.0198 Loss_G: 5.7202 D(x): 0.9850 D(G(z)): 0.0045 / 0.0052
    [23/200][  50/ 507] Loss_D: 0.0276 Loss_G: 5.7011 D(x): 0.9895 D(G(z)): 0.0166 / 0.0052
    [23/200][ 100/ 507] Loss_D: 0.0166 Loss_G: 5.8310 D(x): 0.9926 D(G(z)): 0.0091 / 0.0048
    [23/200][ 150/ 507] Loss_D: 0.0138 Loss_G: 6.3737 D(x): 0.9921 D(G(z)): 0.0058 / 0.0027
    [23/200][ 200/ 507] Loss_D: 0.0140 Loss_G: 6.3191 D(x): 0.9971 D(G(z)): 0.0110 / 0.0029
    [23/200][ 250/ 507] Loss_D: 0.0161 Loss_G: 5.6920 D(x): 0.9917 D(G(z)): 0.0077 / 0.0055
    [23/200][ 300/ 507] Loss_D: 0.0107 Loss_G: 6.0985 D(x): 0.9944 D(G(z)): 0.0050 / 0.0034
    [23/200][ 350/ 507] Loss_D: 0.0201 Loss_G: 5.6264 D(x): 0.9900 D(G(z)): 0.0099 / 0.0048
    [23/200][ 400/ 507] Loss_D: 0.0154 Loss_G: 5.7792 D(x): 0.9891 D(G(z)): 0.0044 / 0.0048
    [23/200][ 450/ 507] Loss_D: 0.0151 Loss_G: 6.0728 D(x): 0.9945 D(G(z)): 0.0094 / 0.0040
    [23/200][ 500/ 507] Loss_D: 0.0108 Loss_G: 5.7711 D(x): 0.9960 D(G(z)): 0.0067 / 0.0047
    [24/200][  50/ 507] Loss_D: 0.0107 Loss_G: 6.4471 D(x): 0.9942 D(G(z)): 0.0048 / 0.0032
    [24/200][ 100/ 507] Loss_D: 0.0095 Loss_G: 6.2439 D(x): 0.9948 D(G(z)): 0.0043 / 0.0031
    [24/200][ 150/ 507] Loss_D: 0.0130 Loss_G: 5.9630 D(x): 0.9930 D(G(z)): 0.0059 / 0.0042
    [24/200][ 200/ 507] Loss_D: 0.0098 Loss_G: 6.2692 D(x): 0.9936 D(G(z)): 0.0034 / 0.0031
    [24/200][ 250/ 507] Loss_D: 0.0136 Loss_G: 6.2428 D(x): 0.9946 D(G(z)): 0.0081 / 0.0031
    [24/200][ 300/ 507] Loss_D: 0.0081 Loss_G: 6.2296 D(x): 0.9968 D(G(z)): 0.0049 / 0.0029
    [24/200][ 350/ 507] Loss_D: 0.0105 Loss_G: 6.5513 D(x): 0.9945 D(G(z)): 0.0048 / 0.0024
    [24/200][ 400/ 507] Loss_D: 0.0095 Loss_G: 6.5723 D(x): 0.9927 D(G(z)): 0.0021 / 0.0020
    [24/200][ 450/ 507] Loss_D: 0.0110 Loss_G: 6.5935 D(x): 0.9941 D(G(z)): 0.0050 / 0.0024
    [24/200][ 500/ 507] Loss_D: 0.0107 Loss_G: 6.3113 D(x): 0.9953 D(G(z)): 0.0059 / 0.0034
    [25/200][  50/ 507] Loss_D: 0.0121 Loss_G: 6.9731 D(x): 0.9972 D(G(z)): 0.0092 / 0.0015
    [25/200][ 100/ 507] Loss_D: 0.0114 Loss_G: 6.7891 D(x): 0.9960 D(G(z)): 0.0072 / 0.0016
    [25/200][ 150/ 507] Loss_D: 0.0078 Loss_G: 6.5535 D(x): 0.9954 D(G(z)): 0.0032 / 0.0023
    [25/200][ 200/ 507] Loss_D: 0.0075 Loss_G: 6.6870 D(x): 0.9965 D(G(z)): 0.0040 / 0.0019
    [25/200][ 250/ 507] Loss_D: 0.0075 Loss_G: 6.3265 D(x): 0.9967 D(G(z)): 0.0042 / 0.0025
    [25/200][ 300/ 507] Loss_D: 0.0141 Loss_G: 7.2224 D(x): 0.9979 D(G(z)): 0.0118 / 0.0010
    [25/200][ 350/ 507] Loss_D: 0.0061 Loss_G: 6.7845 D(x): 0.9987 D(G(z)): 0.0048 / 0.0018
    [25/200][ 400/ 507] Loss_D: 0.0057 Loss_G: 7.1034 D(x): 0.9984 D(G(z)): 0.0041 / 0.0013
    [25/200][ 450/ 507] Loss_D: 0.0093 Loss_G: 7.2311 D(x): 0.9948 D(G(z)): 0.0040 / 0.0017
    [25/200][ 500/ 507] Loss_D: 0.0075 Loss_G: 7.0986 D(x): 0.9961 D(G(z)): 0.0035 / 0.0014
    [26/200][  50/ 507] Loss_D: 0.0058 Loss_G: 7.2843 D(x): 0.9965 D(G(z)): 0.0022 / 0.0012
    [26/200][ 100/ 507] Loss_D: 0.0067 Loss_G: 7.1622 D(x): 0.9983 D(G(z)): 0.0049 / 0.0015
    [26/200][ 150/ 507] Loss_D: 0.0060 Loss_G: 7.4842 D(x): 0.9980 D(G(z)): 0.0039 / 0.0013
    [26/200][ 200/ 507] Loss_D: 0.0050 Loss_G: 7.0138 D(x): 0.9979 D(G(z)): 0.0029 / 0.0015
    [26/200][ 250/ 507] Loss_D: 0.0065 Loss_G: 6.8826 D(x): 0.9962 D(G(z)): 0.0027 / 0.0021
    [26/200][ 300/ 507] Loss_D: 0.0084 Loss_G: 6.8872 D(x): 0.9954 D(G(z)): 0.0037 / 0.0030
    [26/200][ 350/ 507] Loss_D: 2.1285 Loss_G: 3.0200 D(x): 0.8080 D(G(z)): 0.7336 / 0.1189
    [26/200][ 400/ 507] Loss_D: 0.9581 Loss_G: 2.1794 D(x): 0.6747 D(G(z)): 0.3103 / 0.1467
    [26/200][ 450/ 507] Loss_D: 1.0219 Loss_G: 2.1942 D(x): 0.4803 D(G(z)): 0.1215 / 0.1806
    [26/200][ 500/ 507] Loss_D: 0.9801 Loss_G: 5.0796 D(x): 0.7867 D(G(z)): 0.3562 / 0.0103
    [27/200][  50/ 507] Loss_D: 0.4411 Loss_G: 3.8638 D(x): 0.7929 D(G(z)): 0.1328 / 0.0354
    [27/200][ 100/ 507] Loss_D: 0.5769 Loss_G: 4.2179 D(x): 0.8321 D(G(z)): 0.2348 / 0.0270
    [27/200][ 150/ 507] Loss_D: 0.1849 Loss_G: 4.1357 D(x): 0.9246 D(G(z)): 0.0697 / 0.0322
    [27/200][ 200/ 507] Loss_D: 1.3083 Loss_G: 6.2706 D(x): 0.4887 D(G(z)): 0.0053 / 0.0090
    [27/200][ 250/ 507] Loss_D: 0.2962 Loss_G: 5.5670 D(x): 0.9074 D(G(z)): 0.1482 / 0.0067
    [27/200][ 300/ 507] Loss_D: 0.2190 Loss_G: 5.3737 D(x): 0.8981 D(G(z)): 0.0740 / 0.0098
    [27/200][ 350/ 507] Loss_D: 0.2951 Loss_G: 7.4947 D(x): 0.9403 D(G(z)): 0.1675 / 0.0009
    [27/200][ 400/ 507] Loss_D: 0.2726 Loss_G: 4.4394 D(x): 0.8733 D(G(z)): 0.0508 / 0.0258
    [27/200][ 450/ 507] Loss_D: 0.1046 Loss_G: 6.5678 D(x): 0.9793 D(G(z)): 0.0722 / 0.0027
    [27/200][ 500/ 507] Loss_D: 0.0496 Loss_G: 5.7105 D(x): 0.9644 D(G(z)): 0.0110 / 0.0075
    [28/200][  50/ 507] Loss_D: 0.0784 Loss_G: 5.4267 D(x): 0.9599 D(G(z)): 0.0249 / 0.0080
    [28/200][ 100/ 507] Loss_D: 0.0955 Loss_G: 6.6614 D(x): 0.9928 D(G(z)): 0.0771 / 0.0020
    [28/200][ 150/ 507] Loss_D: 0.0542 Loss_G: 5.5983 D(x): 0.9697 D(G(z)): 0.0201 / 0.0071
    [28/200][ 200/ 507] Loss_D: 0.0676 Loss_G: 6.2922 D(x): 0.9920 D(G(z)): 0.0502 / 0.0029
    [28/200][ 250/ 507] Loss_D: 0.0519 Loss_G: 5.3740 D(x): 0.9729 D(G(z)): 0.0226 / 0.0078
    [28/200][ 300/ 507] Loss_D: 0.0359 Loss_G: 5.8347 D(x): 0.9749 D(G(z)): 0.0099 / 0.0059
    [28/200][ 350/ 507] Loss_D: 0.0461 Loss_G: 5.2546 D(x): 0.9670 D(G(z)): 0.0110 / 0.0103
    [28/200][ 400/ 507] Loss_D: 0.0266 Loss_G: 5.3611 D(x): 0.9835 D(G(z)): 0.0097 / 0.0075
    [28/200][ 450/ 507] Loss_D: 0.0271 Loss_G: 5.6018 D(x): 0.9892 D(G(z)): 0.0158 / 0.0056
    [28/200][ 500/ 507] Loss_D: 0.0201 Loss_G: 5.5041 D(x): 0.9936 D(G(z)): 0.0133 / 0.0058
    [29/200][  50/ 507] Loss_D: 0.0181 Loss_G: 5.8920 D(x): 0.9896 D(G(z)): 0.0075 / 0.0050
    [29/200][ 100/ 507] Loss_D: 0.0276 Loss_G: 6.3010 D(x): 0.9916 D(G(z)): 0.0184 / 0.0039
    [29/200][ 150/ 507] Loss_D: 0.0311 Loss_G: 5.3939 D(x): 0.9793 D(G(z)): 0.0098 / 0.0087
    [29/200][ 200/ 507] Loss_D: 0.0341 Loss_G: 5.7835 D(x): 0.9872 D(G(z)): 0.0195 / 0.0052
    [29/200][ 250/ 507] Loss_D: 0.0173 Loss_G: 6.1600 D(x): 0.9881 D(G(z)): 0.0052 / 0.0034
    [29/200][ 300/ 507] Loss_D: 0.0203 Loss_G: 5.7301 D(x): 0.9876 D(G(z)): 0.0076 / 0.0076
    [29/200][ 350/ 507] Loss_D: 0.0152 Loss_G: 5.8043 D(x): 0.9938 D(G(z)): 0.0089 / 0.0052
    [29/200][ 400/ 507] Loss_D: 0.0297 Loss_G: 6.0577 D(x): 0.9977 D(G(z)): 0.0265 / 0.0036
    [29/200][ 450/ 507] Loss_D: 0.0176 Loss_G: 5.8515 D(x): 0.9930 D(G(z)): 0.0104 / 0.0049
    [29/200][ 500/ 507] Loss_D: 0.0128 Loss_G: 6.0382 D(x): 0.9948 D(G(z)): 0.0075 / 0.0040
    [30/200][  50/ 507] Loss_D: 0.0147 Loss_G: 6.2169 D(x): 0.9952 D(G(z)): 0.0097 / 0.0032
    [30/200][ 100/ 507] Loss_D: 0.0169 Loss_G: 5.5425 D(x): 0.9950 D(G(z)): 0.0118 / 0.0062
    [30/200][ 150/ 507] Loss_D: 0.0150 Loss_G: 6.0529 D(x): 0.9902 D(G(z)): 0.0052 / 0.0042
    [30/200][ 200/ 507] Loss_D: 0.0135 Loss_G: 7.0471 D(x): 0.9896 D(G(z)): 0.0030 / 0.0022
    [30/200][ 250/ 507] Loss_D: 0.0090 Loss_G: 6.3034 D(x): 0.9938 D(G(z)): 0.0028 / 0.0029
    [30/200][ 300/ 507] Loss_D: 0.0128 Loss_G: 5.8552 D(x): 0.9944 D(G(z)): 0.0071 / 0.0043
    [30/200][ 350/ 507] Loss_D: 0.0132 Loss_G: 6.5686 D(x): 0.9920 D(G(z)): 0.0050 / 0.0029
    [30/200][ 400/ 507] Loss_D: 0.0130 Loss_G: 5.8756 D(x): 0.9928 D(G(z)): 0.0057 / 0.0039
    [30/200][ 450/ 507] Loss_D: 0.0152 Loss_G: 6.3198 D(x): 0.9921 D(G(z)): 0.0072 / 0.0031
    [30/200][ 500/ 507] Loss_D: 0.0128 Loss_G: 6.2751 D(x): 0.9899 D(G(z)): 0.0026 / 0.0032
    [31/200][  50/ 507] Loss_D: 0.0101 Loss_G: 6.2513 D(x): 0.9938 D(G(z)): 0.0039 / 0.0034
    [31/200][ 100/ 507] Loss_D: 0.0102 Loss_G: 6.6706 D(x): 0.9945 D(G(z)): 0.0046 / 0.0030
    [31/200][ 150/ 507] Loss_D: 0.0130 Loss_G: 6.1953 D(x): 0.9932 D(G(z)): 0.0061 / 0.0039
    [31/200][ 200/ 507] Loss_D: 0.0081 Loss_G: 6.6519 D(x): 0.9962 D(G(z)): 0.0043 / 0.0023
    [31/200][ 250/ 507] Loss_D: 0.0092 Loss_G: 6.5836 D(x): 0.9966 D(G(z)): 0.0056 / 0.0019
    [31/200][ 300/ 507] Loss_D: 0.0077 Loss_G: 6.4989 D(x): 0.9962 D(G(z)): 0.0039 / 0.0026
    [31/200][ 350/ 507] Loss_D: 0.0083 Loss_G: 6.4273 D(x): 0.9961 D(G(z)): 0.0044 / 0.0027
    [31/200][ 400/ 507] Loss_D: 0.0091 Loss_G: 6.4803 D(x): 0.9971 D(G(z)): 0.0062 / 0.0025
    [31/200][ 450/ 507] Loss_D: 0.0132 Loss_G: 6.7866 D(x): 0.9955 D(G(z)): 0.0086 / 0.0017
    [31/200][ 500/ 507] Loss_D: 0.0135 Loss_G: 6.2203 D(x): 0.9894 D(G(z)): 0.0028 / 0.0039
    [32/200][  50/ 507] Loss_D: 0.0109 Loss_G: 6.7536 D(x): 0.9949 D(G(z)): 0.0056 / 0.0020
    [32/200][ 100/ 507] Loss_D: 0.0098 Loss_G: 6.5991 D(x): 0.9955 D(G(z)): 0.0053 / 0.0026
    [32/200][ 150/ 507] Loss_D: 0.0047 Loss_G: 6.8946 D(x): 0.9984 D(G(z)): 0.0030 / 0.0019
    [32/200][ 200/ 507] Loss_D: 0.0074 Loss_G: 6.7234 D(x): 0.9954 D(G(z)): 0.0027 / 0.0021
    [32/200][ 250/ 507] Loss_D: 0.0067 Loss_G: 6.6656 D(x): 0.9970 D(G(z)): 0.0036 / 0.0026
    [32/200][ 300/ 507] Loss_D: 0.0049 Loss_G: 7.0362 D(x): 0.9983 D(G(z)): 0.0031 / 0.0014
    [32/200][ 350/ 507] Loss_D: 0.0069 Loss_G: 7.0615 D(x): 0.9972 D(G(z)): 0.0040 / 0.0014
    [32/200][ 400/ 507] Loss_D: 0.0041 Loss_G: 7.1637 D(x): 0.9980 D(G(z)): 0.0021 / 0.0013
    [32/200][ 450/ 507] Loss_D: 0.0046 Loss_G: 7.3243 D(x): 0.9979 D(G(z)): 0.0025 / 0.0012
    [32/200][ 500/ 507] Loss_D: 0.0105 Loss_G: 7.5512 D(x): 0.9992 D(G(z)): 0.0095 / 0.0007
    [33/200][  50/ 507] Loss_D: 1.2872 Loss_G: 1.9805 D(x): 0.6090 D(G(z)): 0.2881 / 0.2374
    [33/200][ 100/ 507] Loss_D: 1.0077 Loss_G: 1.7211 D(x): 0.6002 D(G(z)): 0.2551 / 0.2184
    [33/200][ 150/ 507] Loss_D: 0.6663 Loss_G: 2.9404 D(x): 0.7045 D(G(z)): 0.1864 / 0.0991
    [33/200][ 200/ 507] Loss_D: 0.9508 Loss_G: 5.9122 D(x): 0.8811 D(G(z)): 0.3646 / 0.0072
    [33/200][ 250/ 507] Loss_D: 0.5388 Loss_G: 5.8609 D(x): 0.8902 D(G(z)): 0.2617 / 0.0051
    [33/200][ 300/ 507] Loss_D: 0.9430 Loss_G: 6.0940 D(x): 0.7551 D(G(z)): 0.2349 / 0.0065
    [33/200][ 350/ 507] Loss_D: 0.4696 Loss_G: 6.4707 D(x): 0.7877 D(G(z)): 0.1326 / 0.0047
    [33/200][ 400/ 507] Loss_D: 0.5040 Loss_G: 5.2341 D(x): 0.8082 D(G(z)): 0.1205 / 0.0106
    [33/200][ 450/ 507] Loss_D: 0.2927 Loss_G: 4.8761 D(x): 0.8095 D(G(z)): 0.0244 / 0.0200
    [33/200][ 500/ 507] Loss_D: 0.1517 Loss_G: 4.9106 D(x): 0.9328 D(G(z)): 0.0585 / 0.0130
    [34/200][  50/ 507] Loss_D: 0.2447 Loss_G: 6.6433 D(x): 0.9185 D(G(z)): 0.1078 / 0.0041
    [34/200][ 100/ 507] Loss_D: 0.1009 Loss_G: 5.6462 D(x): 0.9792 D(G(z)): 0.0718 / 0.0060
    [34/200][ 150/ 507] Loss_D: 0.0837 Loss_G: 6.3902 D(x): 0.9685 D(G(z)): 0.0461 / 0.0047
    [34/200][ 200/ 507] Loss_D: 0.0596 Loss_G: 5.9402 D(x): 0.9686 D(G(z)): 0.0234 / 0.0049
    [34/200][ 250/ 507] Loss_D: 0.0580 Loss_G: 5.0398 D(x): 0.9646 D(G(z)): 0.0203 / 0.0118
    [34/200][ 300/ 507] Loss_D: 0.0701 Loss_G: 5.8232 D(x): 0.9504 D(G(z)): 0.0149 / 0.0066
    [34/200][ 350/ 507] Loss_D: 0.0615 Loss_G: 6.7207 D(x): 0.9910 D(G(z)): 0.0459 / 0.0019
    [34/200][ 400/ 507] Loss_D: 0.0658 Loss_G: 5.2771 D(x): 0.9485 D(G(z)): 0.0086 / 0.0124
    [34/200][ 450/ 507] Loss_D: 0.0441 Loss_G: 5.4945 D(x): 0.9779 D(G(z)): 0.0202 / 0.0069
    [34/200][ 500/ 507] Loss_D: 0.0295 Loss_G: 5.7491 D(x): 0.9892 D(G(z)): 0.0180 / 0.0069
    [35/200][  50/ 507] Loss_D: 0.0310 Loss_G: 4.9687 D(x): 0.9840 D(G(z)): 0.0142 / 0.0096
    [35/200][ 100/ 507] Loss_D: 0.0379 Loss_G: 6.0405 D(x): 0.9888 D(G(z)): 0.0249 / 0.0047
    [35/200][ 150/ 507] Loss_D: 0.0196 Loss_G: 6.4012 D(x): 0.9853 D(G(z)): 0.0046 / 0.0033
    [35/200][ 200/ 507] Loss_D: 0.0186 Loss_G: 5.8784 D(x): 0.9935 D(G(z)): 0.0119 / 0.0053
    [35/200][ 250/ 507] Loss_D: 0.0236 Loss_G: 5.7605 D(x): 0.9914 D(G(z)): 0.0146 / 0.0063
    [35/200][ 300/ 507] Loss_D: 0.0185 Loss_G: 5.8181 D(x): 0.9915 D(G(z)): 0.0098 / 0.0049
    [35/200][ 350/ 507] Loss_D: 0.0128 Loss_G: 6.5637 D(x): 0.9912 D(G(z)): 0.0039 / 0.0025
    [35/200][ 400/ 507] Loss_D: 0.0160 Loss_G: 6.0988 D(x): 0.9944 D(G(z)): 0.0100 / 0.0035
    [35/200][ 450/ 507] Loss_D: 0.0217 Loss_G: 6.2182 D(x): 0.9901 D(G(z)): 0.0115 / 0.0044
    [35/200][ 500/ 507] Loss_D: 0.0148 Loss_G: 5.8227 D(x): 0.9973 D(G(z)): 0.0118 / 0.0045
    [36/200][  50/ 507] Loss_D: 0.0226 Loss_G: 6.3495 D(x): 0.9862 D(G(z)): 0.0082 / 0.0038
    [36/200][ 100/ 507] Loss_D: 0.0149 Loss_G: 6.1781 D(x): 0.9967 D(G(z)): 0.0114 / 0.0037
    [36/200][ 150/ 507] Loss_D: 0.0158 Loss_G: 6.3179 D(x): 0.9915 D(G(z)): 0.0072 / 0.0031
    [36/200][ 200/ 507] Loss_D: 0.0186 Loss_G: 6.3827 D(x): 0.9938 D(G(z)): 0.0119 / 0.0028
    [36/200][ 250/ 507] Loss_D: 0.0165 Loss_G: 6.4593 D(x): 0.9880 D(G(z)): 0.0042 / 0.0034
    [36/200][ 300/ 507] Loss_D: 0.0160 Loss_G: 5.9361 D(x): 0.9885 D(G(z)): 0.0044 / 0.0050
    [36/200][ 350/ 507] Loss_D: 0.0130 Loss_G: 6.0290 D(x): 0.9969 D(G(z)): 0.0098 / 0.0040
    [36/200][ 400/ 507] Loss_D: 0.0083 Loss_G: 6.5599 D(x): 0.9962 D(G(z)): 0.0045 / 0.0028
    [36/200][ 450/ 507] Loss_D: 0.0134 Loss_G: 6.1156 D(x): 0.9968 D(G(z)): 0.0101 / 0.0030
    [36/200][ 500/ 507] Loss_D: 0.0133 Loss_G: 6.4103 D(x): 0.9973 D(G(z)): 0.0104 / 0.0032
    [37/200][  50/ 507] Loss_D: 0.0141 Loss_G: 6.0508 D(x): 0.9950 D(G(z)): 0.0089 / 0.0037
    [37/200][ 100/ 507] Loss_D: 0.0081 Loss_G: 6.4232 D(x): 0.9968 D(G(z)): 0.0049 / 0.0028
    [37/200][ 150/ 507] Loss_D: 0.0111 Loss_G: 7.2487 D(x): 0.9968 D(G(z)): 0.0078 / 0.0018
    [37/200][ 200/ 507] Loss_D: 0.0102 Loss_G: 6.6325 D(x): 0.9910 D(G(z)): 0.0012 / 0.0022
    [37/200][ 250/ 507] Loss_D: 0.0093 Loss_G: 6.4285 D(x): 0.9946 D(G(z)): 0.0038 / 0.0033
    [37/200][ 300/ 507] Loss_D: 0.0081 Loss_G: 6.5045 D(x): 0.9964 D(G(z)): 0.0045 / 0.0028
    [37/200][ 350/ 507] Loss_D: 0.0109 Loss_G: 6.6138 D(x): 0.9921 D(G(z)): 0.0030 / 0.0022
    [37/200][ 400/ 507] Loss_D: 0.0101 Loss_G: 6.3521 D(x): 0.9970 D(G(z)): 0.0070 / 0.0030
    [37/200][ 450/ 507] Loss_D: 0.0060 Loss_G: 6.8640 D(x): 0.9956 D(G(z)): 0.0015 / 0.0018
    [37/200][ 500/ 507] Loss_D: 0.0046 Loss_G: 7.0480 D(x): 0.9985 D(G(z)): 0.0031 / 0.0020
    [38/200][  50/ 507] Loss_D: 0.0109 Loss_G: 6.2280 D(x): 0.9939 D(G(z)): 0.0048 / 0.0032
    [38/200][ 100/ 507] Loss_D: 0.0069 Loss_G: 6.5018 D(x): 0.9961 D(G(z)): 0.0030 / 0.0024
    [38/200][ 150/ 507] Loss_D: 0.0079 Loss_G: 6.9468 D(x): 0.9943 D(G(z)): 0.0022 / 0.0020
    [38/200][ 200/ 507] Loss_D: 0.0153 Loss_G: 7.4726 D(x): 0.9857 D(G(z)): 0.0007 / 0.0017
    [38/200][ 250/ 507] Loss_D: 0.0070 Loss_G: 6.6929 D(x): 0.9960 D(G(z)): 0.0030 / 0.0020
    [38/200][ 300/ 507] Loss_D: 0.0045 Loss_G: 6.9228 D(x): 0.9986 D(G(z)): 0.0030 / 0.0015
    [38/200][ 350/ 507] Loss_D: 0.0063 Loss_G: 6.7554 D(x): 0.9958 D(G(z)): 0.0022 / 0.0021
    [38/200][ 400/ 507] Loss_D: 0.0046 Loss_G: 7.1284 D(x): 0.9980 D(G(z)): 0.0026 / 0.0016
    [38/200][ 450/ 507] Loss_D: 0.0030 Loss_G: 7.4652 D(x): 0.9986 D(G(z)): 0.0015 / 0.0011
    [38/200][ 500/ 507] Loss_D: 0.0051 Loss_G: 7.2718 D(x): 0.9981 D(G(z)): 0.0032 / 0.0016
    [39/200][  50/ 507] Loss_D: 0.0101 Loss_G: 8.5946 D(x): 0.9987 D(G(z)): 0.0083 / 0.0005
    [39/200][ 100/ 507] Loss_D: 0.0043 Loss_G: 7.3426 D(x): 0.9978 D(G(z)): 0.0021 / 0.0013
    [39/200][ 150/ 507] Loss_D: 0.0059 Loss_G: 7.0458 D(x): 0.9987 D(G(z)): 0.0046 / 0.0016
    [39/200][ 200/ 507] Loss_D: 0.0032 Loss_G: 7.2021 D(x): 0.9985 D(G(z)): 0.0016 / 0.0013
    [39/200][ 250/ 507] Loss_D: 0.0026 Loss_G: 7.3370 D(x): 0.9986 D(G(z)): 0.0012 / 0.0010
    [39/200][ 300/ 507] Loss_D: 0.0033 Loss_G: 7.3808 D(x): 0.9987 D(G(z)): 0.0019 / 0.0012
    [39/200][ 350/ 507] Loss_D: 0.0047 Loss_G: 7.1259 D(x): 0.9973 D(G(z)): 0.0021 / 0.0018
    [39/200][ 400/ 507] Loss_D: 0.0040 Loss_G: 7.3577 D(x): 0.9972 D(G(z)): 0.0012 / 0.0014
    [39/200][ 450/ 507] Loss_D: 0.0040 Loss_G: 7.3458 D(x): 0.9985 D(G(z)): 0.0025 / 0.0011
    [39/200][ 500/ 507] Loss_D: 0.0022 Loss_G: 7.9485 D(x): 0.9987 D(G(z)): 0.0009 / 0.0008
    [40/200][  50/ 507] Loss_D: 0.0031 Loss_G: 7.9651 D(x): 0.9975 D(G(z)): 0.0006 / 0.0007
    [40/200][ 100/ 507] Loss_D: 0.0022 Loss_G: 7.4484 D(x): 0.9990 D(G(z)): 0.0012 / 0.0009
    [40/200][ 150/ 507] Loss_D: 0.0043 Loss_G: 7.7244 D(x): 0.9991 D(G(z)): 0.0033 / 0.0009
    [40/200][ 200/ 507] Loss_D: 0.0037 Loss_G: 7.5511 D(x): 0.9978 D(G(z)): 0.0014 / 0.0012
    [40/200][ 250/ 507] Loss_D: 0.0034 Loss_G: 7.7023 D(x): 0.9981 D(G(z)): 0.0014 / 0.0009
    [40/200][ 300/ 507] Loss_D: 0.0033 Loss_G: 7.2943 D(x): 0.9982 D(G(z)): 0.0014 / 0.0012
    [40/200][ 350/ 507] Loss_D: 0.0023 Loss_G: 8.0168 D(x): 0.9987 D(G(z)): 0.0009 / 0.0007
    [40/200][ 400/ 507] Loss_D: 0.0037 Loss_G: 7.7864 D(x): 0.9978 D(G(z)): 0.0015 / 0.0011
    [40/200][ 450/ 507] Loss_D: 0.0040 Loss_G: 7.9218 D(x): 0.9983 D(G(z)): 0.0022 / 0.0009
    [40/200][ 500/ 507] Loss_D: 0.0025 Loss_G: 7.8729 D(x): 0.9990 D(G(z)): 0.0014 / 0.0008
    [41/200][  50/ 507] Loss_D: 0.0027 Loss_G: 8.1455 D(x): 0.9979 D(G(z)): 0.0007 / 0.0006
    [41/200][ 100/ 507] Loss_D: 0.0020 Loss_G: 8.1732 D(x): 0.9985 D(G(z)): 0.0005 / 0.0006
    [41/200][ 150/ 507] Loss_D: 0.0025 Loss_G: 7.9290 D(x): 0.9992 D(G(z)): 0.0017 / 0.0006
    [41/200][ 200/ 507] Loss_D: 0.0029 Loss_G: 8.2377 D(x): 0.9982 D(G(z)): 0.0011 / 0.0006
    [41/200][ 250/ 507] Loss_D: 0.0025 Loss_G: 7.9218 D(x): 0.9992 D(G(z)): 0.0017 / 0.0006
    [41/200][ 300/ 507] Loss_D: 0.0032 Loss_G: 7.5920 D(x): 0.9975 D(G(z)): 0.0006 / 0.0009
    [41/200][ 350/ 507] Loss_D: 0.0028 Loss_G: 7.4907 D(x): 0.9983 D(G(z)): 0.0010 / 0.0010
    [41/200][ 400/ 507] Loss_D: 0.0017 Loss_G: 8.2387 D(x): 0.9989 D(G(z)): 0.0006 / 0.0005
    [41/200][ 450/ 507] Loss_D: 0.0017 Loss_G: 8.5501 D(x): 0.9993 D(G(z)): 0.0010 / 0.0005
    [41/200][ 500/ 507] Loss_D: 1.1443 Loss_G: 2.2734 D(x): 0.6605 D(G(z)): 0.3550 / 0.1657
    [42/200][  50/ 507] Loss_D: 1.0529 Loss_G: 2.6272 D(x): 0.6332 D(G(z)): 0.2914 / 0.1514
    [42/200][ 100/ 507] Loss_D: 1.0425 Loss_G: 2.4217 D(x): 0.5683 D(G(z)): 0.1600 / 0.1464
    [42/200][ 150/ 507] Loss_D: 1.6002 Loss_G: 3.1052 D(x): 0.4465 D(G(z)): 0.0709 / 0.1068
    [42/200][ 200/ 507] Loss_D: 0.5167 Loss_G: 5.9679 D(x): 0.7548 D(G(z)): 0.0232 / 0.0107
    [42/200][ 250/ 507] Loss_D: 0.8810 Loss_G: 4.2548 D(x): 0.8604 D(G(z)): 0.3042 / 0.0201
    [42/200][ 300/ 507] Loss_D: 0.9628 Loss_G: 11.1347 D(x): 0.9358 D(G(z)): 0.4412 / 0.0000
    [42/200][ 350/ 507] Loss_D: 0.3662 Loss_G: 6.8784 D(x): 0.8356 D(G(z)): 0.1113 / 0.0044
    [42/200][ 400/ 507] Loss_D: 0.5551 Loss_G: 7.6342 D(x): 0.8378 D(G(z)): 0.1651 / 0.0010
    [42/200][ 450/ 507] Loss_D: 0.2909 Loss_G: 5.7394 D(x): 0.8522 D(G(z)): 0.0168 / 0.0236
    [42/200][ 500/ 507] Loss_D: 0.1705 Loss_G: 6.1368 D(x): 0.9166 D(G(z)): 0.0092 / 0.0090
    [43/200][  50/ 507] Loss_D: 0.1123 Loss_G: 5.6544 D(x): 0.9307 D(G(z)): 0.0223 / 0.0144
    [43/200][ 100/ 507] Loss_D: 0.1670 Loss_G: 5.9573 D(x): 0.9429 D(G(z)): 0.0712 / 0.0061
    [43/200][ 150/ 507] Loss_D: 0.1569 Loss_G: 5.9076 D(x): 0.8878 D(G(z)): 0.0096 / 0.0115
    [43/200][ 200/ 507] Loss_D: 0.1397 Loss_G: 5.5476 D(x): 0.9394 D(G(z)): 0.0349 / 0.0072
    [43/200][ 250/ 507] Loss_D: 0.0485 Loss_G: 6.8114 D(x): 0.9595 D(G(z)): 0.0051 / 0.0032
    [43/200][ 300/ 507] Loss_D: 0.0558 Loss_G: 6.0355 D(x): 0.9805 D(G(z)): 0.0328 / 0.0054
    [43/200][ 350/ 507] Loss_D: 0.0337 Loss_G: 6.4752 D(x): 0.9854 D(G(z)): 0.0182 / 0.0035
    [43/200][ 400/ 507] Loss_D: 0.0320 Loss_G: 5.3819 D(x): 0.9835 D(G(z)): 0.0139 / 0.0084
    [43/200][ 450/ 507] Loss_D: 0.0576 Loss_G: 5.9486 D(x): 0.9942 D(G(z)): 0.0472 / 0.0041
    [43/200][ 500/ 507] Loss_D: 0.0393 Loss_G: 6.1306 D(x): 0.9795 D(G(z)): 0.0169 / 0.0052
    [44/200][  50/ 507] Loss_D: 0.0629 Loss_G: 4.8906 D(x): 0.9555 D(G(z)): 0.0144 / 0.0158
    [44/200][ 100/ 507] Loss_D: 0.0185 Loss_G: 6.4201 D(x): 0.9958 D(G(z)): 0.0139 / 0.0037
    [44/200][ 150/ 507] Loss_D: 0.0179 Loss_G: 6.8165 D(x): 0.9912 D(G(z)): 0.0089 / 0.0029
    [44/200][ 200/ 507] Loss_D: 0.0817 Loss_G: 4.9673 D(x): 0.9421 D(G(z)): 0.0047 / 0.0140
    [44/200][ 250/ 507] Loss_D: 0.0178 Loss_G: 6.4949 D(x): 0.9869 D(G(z)): 0.0044 / 0.0039
    [44/200][ 300/ 507] Loss_D: 0.0300 Loss_G: 5.7725 D(x): 0.9856 D(G(z)): 0.0149 / 0.0073
    [44/200][ 350/ 507] Loss_D: 0.0274 Loss_G: 5.9215 D(x): 0.9874 D(G(z)): 0.0143 / 0.0051
    [44/200][ 400/ 507] Loss_D: 0.0197 Loss_G: 6.2928 D(x): 0.9918 D(G(z)): 0.0112 / 0.0047
    [44/200][ 450/ 507] Loss_D: 0.0127 Loss_G: 6.5794 D(x): 0.9915 D(G(z)): 0.0041 / 0.0030
    [44/200][ 500/ 507] Loss_D: 0.0197 Loss_G: 6.2921 D(x): 0.9880 D(G(z)): 0.0073 / 0.0041
    [45/200][  50/ 507] Loss_D: 0.0112 Loss_G: 6.4448 D(x): 0.9927 D(G(z)): 0.0038 / 0.0031
    [45/200][ 100/ 507] Loss_D: 0.0130 Loss_G: 6.3825 D(x): 0.9944 D(G(z)): 0.0073 / 0.0038
    [45/200][ 150/ 507] Loss_D: 0.0181 Loss_G: 6.4844 D(x): 0.9854 D(G(z)): 0.0032 / 0.0030
    [45/200][ 200/ 507] Loss_D: 0.0095 Loss_G: 6.4869 D(x): 0.9950 D(G(z)): 0.0044 / 0.0029
    [45/200][ 250/ 507] Loss_D: 0.0068 Loss_G: 6.3154 D(x): 0.9975 D(G(z)): 0.0043 / 0.0030
    [45/200][ 300/ 507] Loss_D: 0.0072 Loss_G: 6.9857 D(x): 0.9948 D(G(z)): 0.0020 / 0.0018
    [45/200][ 350/ 507] Loss_D: 0.0244 Loss_G: 7.4024 D(x): 0.9958 D(G(z)): 0.0195 / 0.0016
    [45/200][ 400/ 507] Loss_D: 0.0092 Loss_G: 6.3351 D(x): 0.9959 D(G(z)): 0.0050 / 0.0031
    [45/200][ 450/ 507] Loss_D: 0.0166 Loss_G: 6.0088 D(x): 0.9892 D(G(z)): 0.0055 / 0.0048
    [45/200][ 500/ 507] Loss_D: 0.0135 Loss_G: 6.5935 D(x): 0.9935 D(G(z)): 0.0068 / 0.0022
    [46/200][  50/ 507] Loss_D: 0.0082 Loss_G: 7.0824 D(x): 0.9938 D(G(z)): 0.0020 / 0.0020
    [46/200][ 100/ 507] Loss_D: 0.0076 Loss_G: 7.0342 D(x): 0.9943 D(G(z)): 0.0018 / 0.0016
    [46/200][ 150/ 507] Loss_D: 0.0060 Loss_G: 6.6176 D(x): 0.9980 D(G(z)): 0.0040 / 0.0024
    [46/200][ 200/ 507] Loss_D: 0.0151 Loss_G: 6.9520 D(x): 0.9891 D(G(z)): 0.0039 / 0.0020
    [46/200][ 250/ 507] Loss_D: 0.0098 Loss_G: 7.0459 D(x): 0.9929 D(G(z)): 0.0026 / 0.0028
    [46/200][ 300/ 507] Loss_D: 0.0085 Loss_G: 6.7728 D(x): 0.9966 D(G(z)): 0.0050 / 0.0021
    [46/200][ 350/ 507] Loss_D: 0.0059 Loss_G: 7.0155 D(x): 0.9975 D(G(z)): 0.0034 / 0.0020
    [46/200][ 400/ 507] Loss_D: 0.0051 Loss_G: 7.4582 D(x): 0.9975 D(G(z)): 0.0026 / 0.0014
    [46/200][ 450/ 507] Loss_D: 0.0072 Loss_G: 6.7807 D(x): 0.9951 D(G(z)): 0.0022 / 0.0020
    [46/200][ 500/ 507] Loss_D: 0.0084 Loss_G: 7.4826 D(x): 0.9985 D(G(z)): 0.0068 / 0.0014
    [47/200][  50/ 507] Loss_D: 0.0048 Loss_G: 7.1090 D(x): 0.9972 D(G(z)): 0.0020 / 0.0016
    [47/200][ 100/ 507] Loss_D: 0.0051 Loss_G: 7.4067 D(x): 0.9974 D(G(z)): 0.0025 / 0.0013
    [47/200][ 150/ 507] Loss_D: 0.0044 Loss_G: 7.6970 D(x): 0.9974 D(G(z)): 0.0018 / 0.0010
    [47/200][ 200/ 507] Loss_D: 0.0070 Loss_G: 7.1940 D(x): 0.9963 D(G(z)): 0.0033 / 0.0013
    [47/200][ 250/ 507] Loss_D: 0.0066 Loss_G: 7.5486 D(x): 0.9966 D(G(z)): 0.0031 / 0.0012
    [47/200][ 300/ 507] Loss_D: 0.0066 Loss_G: 7.4618 D(x): 0.9955 D(G(z)): 0.0020 / 0.0013
    [47/200][ 350/ 507] Loss_D: 0.0070 Loss_G: 7.5678 D(x): 0.9973 D(G(z)): 0.0042 / 0.0016
    [47/200][ 400/ 507] Loss_D: 0.0035 Loss_G: 8.2213 D(x): 0.9984 D(G(z)): 0.0019 / 0.0012
    [47/200][ 450/ 507] Loss_D: 0.0041 Loss_G: 7.7389 D(x): 0.9987 D(G(z)): 0.0028 / 0.0010
    [47/200][ 500/ 507] Loss_D: 0.0029 Loss_G: 8.6119 D(x): 0.9980 D(G(z)): 0.0009 / 0.0005
    [48/200][  50/ 507] Loss_D: 0.0035 Loss_G: 8.6057 D(x): 0.9974 D(G(z)): 0.0008 / 0.0006
    [48/200][ 100/ 507] Loss_D: 0.0025 Loss_G: 7.6364 D(x): 0.9993 D(G(z)): 0.0018 / 0.0011
    [48/200][ 150/ 507] Loss_D: 0.0021 Loss_G: 8.2745 D(x): 0.9986 D(G(z)): 0.0007 / 0.0004
    [48/200][ 200/ 507] Loss_D: 0.0015 Loss_G: 8.4507 D(x): 0.9994 D(G(z)): 0.0009 / 0.0005
    [48/200][ 250/ 507] Loss_D: 0.0042 Loss_G: 8.7731 D(x): 0.9969 D(G(z)): 0.0010 / 0.0004
    [48/200][ 300/ 507] Loss_D: 0.0025 Loss_G: 9.7029 D(x): 0.9990 D(G(z)): 0.0015 / 0.0001
    [48/200][ 350/ 507] Loss_D: 0.0038 Loss_G: 11.8794 D(x): 0.9998 D(G(z)): 0.0036 / 0.0000
    [48/200][ 400/ 507] Loss_D: 0.0033 Loss_G: 9.3546 D(x): 0.9974 D(G(z)): 0.0006 / 0.0002
    [48/200][ 450/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [48/200][ 500/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][  50/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 100/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 150/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 200/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 250/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 300/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 350/ 507] Loss_D: 0.0001 Loss_G: 27.6310 D(x): 0.9999 D(G(z)): 0.0000 / 0.0000
    [49/200][ 400/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 450/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [49/200][ 500/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][  50/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 100/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 150/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 200/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 250/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 300/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 350/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 400/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 450/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [50/200][ 500/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][  50/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 100/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 150/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 200/ 507] Loss_D: -0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 250/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 300/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 350/ 507] Loss_D: -0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 400/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 450/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [51/200][ 500/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][  50/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 100/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 150/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 200/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 250/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 300/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 350/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 400/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 450/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [52/200][ 500/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [53/200][  50/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [53/200][ 100/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [53/200][ 150/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [53/200][ 200/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [53/200][ 250/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
    [53/200][ 300/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000


    Process Process-224:
    Process Process-221:
    Process Process-223:
    Traceback (most recent call last):
    Traceback (most recent call last):
    Process Process-222:
    Traceback (most recent call last):
    Traceback (most recent call last):
      File "/usr/lib/python3.6/multiprocessing/process.py", line 258, in _bootstrap
        self.run()
      File "/usr/lib/python3.6/multiprocessing/process.py", line 258, in _bootstrap
        self.run()
      File "/usr/lib/python3.6/multiprocessing/process.py", line 93, in run
        self._target(*self._args, **self._kwargs)
      File "/usr/local/lib/python3.6/dist-packages/torch/utils/data/dataloader.py", line 52, in _worker_loop
        r = index_queue.get()
      File "/usr/lib/python3.6/multiprocessing/process.py", line 93, in run
        self._target(*self._args, **self._kwargs)
      File "/usr/lib/python3.6/multiprocessing/process.py", line 258, in _bootstrap
        self.run()



    ---------------------------------------------------------------------------

    KeyboardInterrupt                         Traceback (most recent call last)

    <ipython-input-31-33d03ea61067> in <module>()
         35         errG.backward()
         36         D_G_z2 = output.mean().item()
    ---> 37         optimizerG.step()
         38 
         39         if (i + 1) % 50 == 0:


    /usr/local/lib/python3.6/dist-packages/torch/optim/adam.py in step(self, closure)
         91                 # Decay the first and second moment running average coefficient
         92                 exp_avg.mul_(beta1).add_(1 - beta1, grad)
    ---> 93                 exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)
         94                 if amsgrad:
         95                     # Maintains the maximum of all 2nd moment running avg. till now


    KeyboardInterrupt: 


      File "/usr/lib/python3.6/multiprocessing/process.py", line 258, in _bootstrap
        self.run()
      File "/usr/lib/python3.6/multiprocessing/process.py", line 93, in run
        self._target(*self._args, **self._kwargs)
      File "/usr/lib/python3.6/multiprocessing/queues.py", line 335, in get
        res = self._reader.recv_bytes()
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 216, in recv_bytes
        buf = self._recv_bytes(maxlength)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 407, in _recv_bytes
        buf = self._recv(4)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 379, in _recv
        chunk = read(handle, remaining)
    KeyboardInterrupt
      File "/usr/lib/python3.6/multiprocessing/process.py", line 93, in run
        self._target(*self._args, **self._kwargs)
      File "/usr/local/lib/python3.6/dist-packages/torch/utils/data/dataloader.py", line 52, in _worker_loop
        r = index_queue.get()
      File "/usr/local/lib/python3.6/dist-packages/torch/utils/data/dataloader.py", line 52, in _worker_loop
        r = index_queue.get()
      File "/usr/lib/python3.6/multiprocessing/queues.py", line 335, in get
        res = self._reader.recv_bytes()
      File "/usr/local/lib/python3.6/dist-packages/torch/utils/data/dataloader.py", line 52, in _worker_loop
        r = index_queue.get()
      File "/usr/lib/python3.6/multiprocessing/queues.py", line 335, in get
        res = self._reader.recv_bytes()
      File "/usr/lib/python3.6/multiprocessing/queues.py", line 335, in get
        res = self._reader.recv_bytes()
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 216, in recv_bytes
        buf = self._recv_bytes(maxlength)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 216, in recv_bytes
        buf = self._recv_bytes(maxlength)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 216, in recv_bytes
        buf = self._recv_bytes(maxlength)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 407, in _recv_bytes
        buf = self._recv(4)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 407, in _recv_bytes
        buf = self._recv(4)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 407, in _recv_bytes
        buf = self._recv(4)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 379, in _recv
        chunk = read(handle, remaining)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 379, in _recv
        chunk = read(handle, remaining)
      File "/usr/lib/python3.6/multiprocessing/connection.py", line 379, in _recv
        chunk = read(handle, remaining)
    KeyboardInterrupt
    KeyboardInterrupt
    KeyboardInterrupt

