# Ausgabe

Hier ein paar Beispiele, die von diesem Netz generiert wurden.

## nach einem Durchlauf

![001](epoch_001_generated.png)

## nach 20 Durchläufen

![020](epoch_020_generated.png)

## nach 40 Durchläufen

![040](epoch_040_generated.png)

## nach 46 Durchläufen

![046](epoch_046_generated.png)

## letztes *schönes* Bild nach 47 Durchläufen

![047](epoch_047_generated.png)

## Nach dem Kollaps des Netzes ändert sich nichts mehr ...

![048](epoch_048_generated.png)

## Weiters

Nach jedem Durchlauf wurde der Zustand des Netzes gespeichert.  Der letzte stabile Zustand ist in diesem Verzeichnis zu finden.
Das sind die beiden Dateien `netG_epoch_047.pth` und `netD_epoch_047.pth` in diesem Verzeichnis.  Die können in das [Modell](../dcgan.ipynb) geladen werden, um von diesem Checkpoint aus weiterzumachen.
