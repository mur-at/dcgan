# dcgan

Erste Schritte in Richtung Generierung von Bildern mittels DCGAN.

## Jupyter Notebook

Das Jupyter Notebook mit dem Code ist [hier](dcgan.ipynb) zu finden.  Damit
wurden die Bilder in [results](results/) generiert.
[Hier](results/results.md) ist noch eine Übersichtsseite mit ausgewählten
Beispielbildern.  Der Output der letzten Zelle des Modells ist in der
Darstellung auf Gitlab abgeschnitten.  Wird das Notebook geladen, sollte
auch der Kollaps des Netzes im 48. Durchlauf sichtbar werden.

```
[48/200][ 450/ 507] Loss_D: 0.0000 Loss_G: 27.6310 D(x): 1.0000 D(G(z)): 0.0000 / 0.0000
```

Im 48. Durchlauf ist das Netz dann kollabiert.  [Bild Nr.
47](results/epoch_047_generated.png) ist das letzte *vernünftige* Bild, das
generiert wurde.

Als Input wurden die Orchideenbilder verwendet.  Zuerst wurden alle Bilder
entfernt, auf denen mehr als eine Blüte zu sehen war.  Die Bilder wurden
dann auf ca. 64px verkleinert und dann als Input für das Netz verwendet.
Damit auch eine *vernünftige* Datenmenge zur Verfügung steht, wurden die
Bilder statt einmal gleich zehn mal an das Modell übergeben.

Das Netz läuft in einem Docker Container, der von
[hier](https://github.com/ufoym/deepo) stammt.
